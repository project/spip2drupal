<?php

// If using php > 5.1 the timezone must be set.
// See the valid timezones http://es2.php.net/manual/en/timezones.php
if (version_compare(phpversion(), '5.1', '>=')) {
  date_default_timezone_set('UTC');
}

/**
 * $GLOBALS['puce'] #TODO#
 *
 * This is a code prepended to listings. By default it uses an image.
 * "<li>" is proposed as an alternative in spip documentation.
 * It does the work but breaks html validation (no closing </li> and no enclosed in <ul></ul>)
 *
 * SPIP version ?
 */
$GLOBALS['puce'] = "<li>";

/**
 * $GLOBALS['class_spip']
 *
 * This is the class used in all the paragraphs (<p class="spip">)
 * Default value: ' class="spip"'
 *
 * SPIP version > 2
 */
$GLOBALS['class_spip'] = '';

/**
 * $GLOBALS['debut_intertitre']
 *
 * Default value: "\n<h3 class=\"spip\">"
 *
 * SPIP version > 1.9 ?
 */
$GLOBALS['debut_intertitre'] = "\n<h3>";

/**
 * $GLOBALS['fin_intertitre']
 *
 * Default value: "</h3>"
 *
 * SPIP version > 1.9 ?
 */
//$GLOBALS['fin_intertitre'] = "</h3>\n";

/**
 * $GLOBALS['ligne_horizontale']
 *
 * Default value: "\n<hr class=\"spip\" />\n"
 *
 * SPIP version ?
 */
$GLOBALS['ligne_horizontale'] = "\n<hr>\n";

/**
 * $GLOBALS['ouvre_ref']
 *
 * Default value: '&nbsp;['
 *
 * SPIP version ?
 */
//$GLOBALS['ouvre_ref'] = '&nbsp;[';

/**
 * $GLOBALS['ferme_ref']
 *
 * Default value: ']'
 *
 * SPIP version ?
 */
//$GLOBALS['ferme_ref'] = ']';

/**
 * $GLOBALS['']
 *
 * Default value: 
 *
 * SPIP version ?
 */
//$GLOBALS['ouvre_note'] = '[';

/**
 * $GLOBALS['ferme_note']
 *
 * Default value: '] '
 *
 * SPIP version ?
 */
//$GLOBALS['ferme_note'] = '] ';

/**
 * $GLOBALS['les_notes']
 *
 * Default value: ''
 *
 * SPIP version ?
 */
//$GLOBALS['les_notes'] = '';

?>
