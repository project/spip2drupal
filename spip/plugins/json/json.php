<?php

function timestamp($date) {
  return strtotime($date);
}

function balise_ID_AUTEUR_ARTICLE($p) {
  $_id_article = interprete_argument_balise(1,$p);
  if (!$_id_article) {
    $_id_article = champ_sql('id_article',$p);
  }
  $nom = $p->id_boucle;
  $table = $p->boucles[$nom]->type_requete;
  $p->code = "json_sql_auteur_$table($_id_article)";
  
  $p->interdire_scripts = false;
  return $p;
}

if (function_exists('spip_abstract_fetsel')) {
  define(SQL_FETSEL, 'spip_abstract_fetsel');
}
else {
  define(SQL_FETSEL, 'sql_fetsel');
}

function json_sql_auteur_articles($_id_article) {
  $row = call_user_func(SQL_FETSEL, 'id_auteur', 'spip_auteurs_articles', array('id_article='.intval($_id_article)), '', array(), '1');
  return $row['id_auteur'];
}

function json_output($data) {
  if (($GLOBALS['meta']['charset'] != 'utf-8')&&(!is_null($data))) {
    foreach ($data as $key => $value) {
      if (is_string($value)) {
        $data[$key] = unicode_to_utf_8(charset2unicode($value));
      }
    }
  }

  print json_encode($data);
}

// For php < 5.2. 
// Copied from  http://au.php.net/manual/en/function.json-encode.php#82904
if (!function_exists('json_encode'))
{
  function json_encode($a=false)
  {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return floatval(str_replace(",", ".", strval($a)));
      }

      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return $a;
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a))
    {
      if (key($a) !== $i)
      {
        $isList = false;
        break;
      }
    }
    $result = array();
    if ($isList)
    {
      foreach ($a as $v) $result[] = json_encode($v);
      return '[' . join(',', $result) . ']';
    }
    else
    {
      foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
      return '{' . join(',', $result) . '}';
    }
  }
}
?>
