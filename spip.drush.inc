<?php

/**
 * Implementation of hook_command().
 */
function spip_drush_command() {
  $items = array();

  $items['spip-sql-cli'] = array(
    'description' => 'Connects to mysql cli with spip credentials.',
    'callback' => 'drush_spip_sql_cli_callback',
  );

  return $items;
}

/**
 * Command callback.
 */
function drush_spip_sql_cli_callback() {
  $db_url = 'mysqli://';
  $db_url .= variable_get('spip_dbuser', NULL);
  $db_url .= ':';
  $db_url .= variable_get('spip_dbpass', NULL);
  $db_url .= '@';
  $db_url .= variable_get('spip_dbhost', NULL);
  $db_url .= '/';
  $db_url .= variable_get('spip_dbname', NULL);
  $GLOBALS['db_url'] = $db_url;
  drush_sql_cli();
}
