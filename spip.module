<?php

/**
 * @file
 * provide the interface to collect SPIP\'s database credentials and connecting to it.
 */

/**
 * Implementation of hook_help().
 */
function spip_help($path, $arg) {
  switch ($path) {
    case 'admin/help#spip':
      $output = t('<p>Assistant module used by spip2drupal and spipauth. This module provide the interface to collect SPIP\'s database credentials and connecting to it.</p>');
      break;
  }
  return $output;
}

/**
 * Implementation of hook_menu().
 */
function spip_menu() { 
  $items['admin/settings/spip'] = array(
    'title' => 'SPIP',
    'description' => 'Configure the settings to connect to a SPIP\'s site database.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('spip_settings_form'),
    'access arguments' => array('administer site configuration'),
  );
  $items['admin/settings/spip/database'] = array(
   'title' => 'Database credentials',
   'type' => MENU_DEFAULT_LOCAL_TASK,
   'weight' => -1
  );

  return $items;
}

/**
 * Builder of a form to collect SPIP's database credentials.
 */
function spip_settings_form(&$form_state) {
  $form['db'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database configuration'),
  );
  $form['db']['spip_dbhost'] = array(
    '#type' => 'textfield',
    '#title' => t('Database host'),
    '#default_value' => variable_get('spip_dbhost', 'localhost'),
    '#required' => TRUE,
  );
  $form['db']['spip_dbname'] = array(
    '#type' => 'textfield',
    '#title' => t('Database name'),
    '#default_value' => variable_get('spip_dbname', ''),
    '#required' => TRUE,
  );
  $form['db']['spip_dbuser'] = array(
    '#type' => 'textfield',
    '#title' => t('Database user'),
    '#default_value' => variable_get('spip_dbuser', ''),
    '#required' => TRUE,
  );
  $form['db']['spip_dbpass'] = array(
    '#type' => 'password',
    '#title' => t('Database password'),
    '#default_value' => variable_get('spip_dbpass', ''),
    '#required' => TRUE,
  );
  $form['db']['spip_version'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('spip_version', ''),
  );

  $form = system_settings_form($form);
  return $form;
}

/**
 * Validate given values trying a connection to the database.
 *
 * If success, query the database to get the SPIP version and alter the 
 * form value to pass to be stored upon submission.
 */
function spip_settings_form_validate($form, &$form_state) {
  // Do not do our validation if there are other errors (rerquired fields).
  if (!is_null(form_get_errors())) {
    return;
  }

  $dbhost = $form_state['values']['spip_dbhost'];
  $dbuser = $form_state['values']['spip_dbuser'];
  $dbpass = $form_state['values']['spip_dbpass'];
  $dbname = $form_state['values']['spip_dbname'];
  $link = @mysql_connect($dbhost, $dbuser, $dbpass, TRUE, 2);
  if (!$link) {
    form_set_error('', t('Unable to connect to database at %host with user %user and password %pass.', array('%host' => $dbhost, '%user' => $dbuser, '%pass' => $dbpass)));
  }
  else {
    drupal_set_message('Connection to database server: success.');
    if (!mysql_select_db($dbname)) {
      form_set_error('', t('Unable to select database :db.', array(':db' => $dbname)));
    }
    else {
      drupal_set_message('Connection to spip database: success.');
      // get the spip version
      $rr = mysql_query("SELECT valeur FROM spip_meta WHERE nom='version_installee'", $link);
      $version = (float)mysql_result($rr, 0);
      drupal_set_message(t('SPIP version found: %version.', array('%version' => $version)));
      form_set_value($form['db']['spip_version'], $version, $form_state);
    }
  }
}

/**
 * Connect to SPIP's database.
 * @return
 *    Mysql link or FALSE.
 */
function spip_db_connect() {
  static $link = FALSE;
  
  if (!$link) {
    $dbhost = variable_get('spip_dbhost', NULL);
    $dbuser = variable_get('spip_dbuser', NULL);
    $dbpass = variable_get('spip_dbpass', NULL);
    $dbname = variable_get('spip_dbname', NULL);

    $link = @mysql_connect($dbhost, $dbuser, $dbpass, TRUE, 2);
    if (!$link) {
      drupal_set_message(t('Unable to connect to database at «%host» with user «%user» and password «%pass».', array('%host' => $dbhost, '%user' => $dbuser, '%pass' => $dbpass)), 'error');
    }
    elseif (!mysql_select_db($dbname)) {
        drupal_set_message(t('Unable to select database :db.', array(':db' => $dbname)), 'error');
        $link = FALSE;
    }
  }

  return $link;
}
