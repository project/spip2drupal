This file try to be a concise to fit requirements and start using the
module. You will find more info in the module's project page.

== requirements ==
=== json ===
This module provides a json plugin for spip. It connects to spip via
web in order to retrive parsed contents in json format. To acomplish
this, spip and drupal sites doesn't need to be in the same server,
although being in the same server or network will speed up the migration
process.

JSON is included in php since 5.2. This is not a requirement for the spip
site because an alternative implementation of json_encode is provided in
the plugin.
I don't have at hand a reliable implementation of json_decode so the drupal
module requires php 5.2 or installing the PECL's json for php < 5.2 in order
to work.

To install the plugin in spip:
 1. Copy the directory spip/plugins/json to the plugins directory of your spip
 site and enable it in the administration interface.
 2. copy the file mes_options.php to you spip's config/ dir. If you
 already have this file just merge them. This file is not strictly
 a requirement but it provide some tweaks to make a better migration.

=== access to the spip database ===
Obviously the module need to connect to the spip database. It can be
local or remote.


== How to use ==
0. Copy IMG/ directory to drupal's files directory.
1. Enable modules: spip, spipauth, spip2drupal.
2. admin > site configuration > configure spip settings.
3. admin > content management > migrate spip to drupal.
4. If the migration process is succesful. Disable spip2drupal module.
5. Ask the users to log to the drupal site for the first time in order to store
  their passwords in drupal database (as spip passwords cannot be imported).
  Users that has not already logged in to drupal for the first time have an
  empty password in users table.
6. When all the users have already logged in for the first time, you can
  disable all modules.

