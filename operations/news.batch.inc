<?php

require_once 'base.batch.inc';

/**
 * Batch operation. Import SPIP breves as drupal breve nodes.
 */
class spipImportNews extends BaseBatchOperation implements iBatchOperation {
  public $token = 'news';

  function __construct(&$context, $step) {
    $this->node_type = variable_get('spip2drupal_node_type_news', '');

    // Daddy does the job.
    parent::__construct($context, $step);
  }

  function count() {
    $query = 'SELECT COUNT(id_breve) FROM {breves}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;

  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {breves} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      // do something

      $this->updateContext();
    }
  }
}
