<?php

interface iBatchOperation {
  public function count();
  public function process($current, $total);
}

class BaseBatchOperation {
  static function execute(&$context, $step = 20) {
     $class = get_called_class();
     $operation = new $class($context, $step);
  }

  function __construct(&$context, $step) {
    $this->spipUrl = variable_get('spip2drupal_spip_url', '');
    $this->dbPrefix = variable_get('spip_tableprefix', 'spip').'_';
    $dbhost = variable_get('spip_dbhost', NULL);
    $dbuser = variable_get('spip_dbuser', NULL);
    $dbpass = variable_get('spip_dbpass', NULL);
    $dbname = variable_get('spip_dbname', NULL);

    $this->dbConnect($dbhost, $dbuser, $dbpass, $dbname);
    $this->context = &$context;
    $this->step = $step;
    // If it is the first time, initialize $context.
    if (empty($context['sandbox'])) {
      $context['sandbox']['total'] = $this->count();
      $context['sandbox']['current'] = 0;
      $context['results']['sections'] = 0;
      if ($context['sandbox']['total'] == 0) {
        $context['finished'] = 1;
        return;
      }
    }

    // Execute the operation.
    $this->process($context['sandbox']['current'], $context['sandbox']['total']);

    // Update finished value 0..1
    $context['finished'] = $context['sandbox']['current'] / $context['sandbox']['total'];
    if ($context['finished'] > 1) {
      drupal_set_message('Race condition: finished > 1. It happen if the total of articles/users or any object has changed while running the batch process', 'warning');
      $context['finished'] = 1;
    }
  }

  /**
   * Update the $context upon each iteration of this operation.
   *
   * This function is to be called at the end of each iteration in the operation
   * loop.
   */
  function updateContext() {
    $this->context['results'][$this->token]++;
    $this->context['sandbox']['current']++;
    $current = $context['sandbox']['current'];
    $this->context['message'] = t('Importing @token @current of @total.', array(
      '@token' => $this->token,
      '@current' => $this->context['sandbox']['current'],
      '@total' => $this->context['sandbox']['total']
      ));
  }

  function dbConnect($dbhost, $dbuser, $dbpass, $dbname) {
    $link = @mysql_connect($dbhost, $dbuser, $dbpass, TRUE, 2);
    if (!$link) {
      drupal_set_message(t('Unable to connect to database at «%host» with user «%user» and password «%pass».', array('%host' => $dbhost, '%user' => $dbuser, '%pass' => $dbpass)), 'error');
    }
    elseif (!mysql_select_db($dbname)) {
        drupal_set_message(t('Unable to select database :db.', array(':db' => $dbname)), 'error');
        $link = FALSE;
    }
    if (!$link) {
      form_set_error('', t("Unable to stablish a connection to SPIP database. Review the settings in the <a href='@url'>database configuration page</a>.", array('@url'=>url('admin/settings/spip/database'))));
    }
    mysql_query('SET NAMES "utf8"', $link);
    $this->link = $link;
  }

  /**
   * Executes a query on the spip database.
   *
   * The query is in drupal format, that is, you need to use {} around the table
   * names.
   */
  function dbQuery($query) {
    $args = func_get_args();
    array_shift($args);
    if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
      $args = $args[0];
    }
    _db_query_callback($args, TRUE);
    $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
    $query = strtr($query, array('{' => $this->dbPrefix, '}' => ''));
    $result = mysql_query($query, $this->link);

    if (!mysql_errno($this->link)) {
      return $result;
    }
    else {
      // Indicate to drupal_error_handler that this is a database error.
      ${DB_ERROR} = TRUE;
      trigger_error(check_plain(mysql_error($this->link) ."\nThe query was: ". $query), E_USER_WARNING);
      return FALSE;
    }
  }

  /**
   * Fetch a spip object of given type.
   */
  function fetch($arg, $value) {
    $row = db_fetch_object(db_query("SELECT data FROM {spip_cache} WHERE arg = '%s' AND value = %d", $arg, $value));
    if ($row === FALSE) {
      $data = $this->_fetch($arg, $value);
      $serialized = serialize($data);
      $record = array('arg' => $arg, 'value' => $value, 'data' => $serialized);
      drupal_write_record('spip_cache', $record);
    }
    else {
      $data = unserialize($row->data);
    }
    return $data;
  }

  /**
   * Connect to spip and fetch an object in json format.
   */
  function _fetch($arg, $value) {
    $url = $this->spipUrl."?page=json&$arg=$value&var_mode=calcul";
    $result = drupal_http_request($url);
    if ($result->code >= 400) {
      drupal_set_message(t('Unable to fetch: !url', array('!url' => $url)), 'error');
      return FALSE;
    }
    $data = json_decode($result->data);
    if (!is_object($data)) {
      $dump = var_export($result->data, TRUE);
      drupal_set_message(t('Empty result for: !url<br/>Dump:<pre>!dump</pre>', array('!url' => $url, '!dump' => $dump)), 'error');
      return FALSE;
    }
    foreach ($data as $key => $value) {
      if (is_string($value)) {
        $data->$key = decode_entities($value);
      }
    }

    return $data;
  }
}
