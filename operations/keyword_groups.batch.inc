<?php

require_once 'base.batch.inc';

/**
 * Batch operations. Import SPIP keyword groups as vocabularies.
 *
 * TODO: texte, unseul?
 */
class spipImportKeywordGroups extends BaseBatchOperation implements iBatchOperation {
  public $token = 'keyword_groups';

  function count() {
    $query = 'SELECT COUNT(id_groupe) FROM {groupes_mots}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {groupes_mots} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_groupe', $row->id_groupe);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }
      if ($data->articles == 'oui') {
        $nodes['story'] = 1;
      }
      $vocab = array(
        'nodes' => $nodes,
        'name' => $data->titre,
        'description' => $data->descriptif,
        'relations' => 1,
        'hierarchy' => 1,
        'multiple' => 0,
        'required' => ($data->obligatoire=='non')?0:1,
        'tags' => 0,
      );
      taxonomy_save_vocabulary($vocab);

      // Store the mapping.
      $record = array('vid' => $vocab['vid'], 'id_groupe' => $row->id_groupe);
      drupal_write_record('spip_keywords_groups', $record);

      $this->updateContext();
    }
  }
}
