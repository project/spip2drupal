<?php

require_once 'base.batch.inc';

/**
 * Batch operation. Import SPIP users as drupal users.
 *
 * TODO: SPIP database fields: bio, nom_site, url_site, pgp, htpass, imessage, messagerie,alea_actuel, alea_futur, prefs, source, lang, extra, low_sec, idx
 */
class spipImportUsers extends BaseBatchOperation implements iBatchOperation {
  public $token = 'users';

  function count() {
    $query = 'SELECT COUNT(id_auteur) FROM {auteurs}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function process($current, $total) {
    $roles = array(
      '0minirezo' => variable_get('spip2drupal_roles_admin', NULL),
      '1comite' => variable_get('spip2drupal_roles_editor', NULL)
    );

    $query = "SELECT id_auteur, login FROM {auteurs} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $uid = $this->userExist($row->login);
      if ($uid === FALSE) {
        $data = $this->fetch('id_auteur', $row->id_auteur);
        if ($data === FALSE) {
          $this->context['sandbox']['current']++;
          continue;
        }
        // Prepare the user to add.
        $account = array(
          'name' => $data->login,
          'mail' => $data->email,
          'created' => $data->maj,
          'access' => $data->en_ligne,
          'login' => $data->maj,
          'status' => ($data->statut == '5poubelle')?0:1,
          'roles' => isset($roles[$data->statut])?array($roles[$data->statut]):array()
        );
        // Save the user.
        $account = user_save(NULL, $account);
        $uid = $account->uid;
      }

      // Store the mapping.
      $record = array('id_auteur' => $row->id_auteur, 'uid' => $uid);
      drupal_write_record('spip_users', $record);

      $this->updateContext();
    }
  }

  /**
   * Returns the uid for $name or FALSE.
   */
  function userExist($name) {
    $rsc = db_query("SELECT uid FROM {users} u WHERE name = '%s'", $name);
    if ($user = db_fetch_object($rsc)) {
      return $user->uid;
    }
    return FALSE;
  }
}
