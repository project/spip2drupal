<?php

require_once 'base.batch.inc';

/**
 * Batch operations. Import SPIP sections as taxonomy terms.
 *
 * TODO: section logo (using taxonomy_image), texte
 */
class spipImportSections extends BaseBatchOperation implements iBatchOperation {
  public $token = 'sections';

  function count() {
    $query = "SELECT COUNT(id_rubrique) FROM {rubriques} WHERE statut='publie'";
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }

  function get($current) {
    $return = array();

    $query = "SELECT id_rubrique FROM {rubriques} WHERE statut='publie' LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_rubrique', $row->id_rubrique);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }
    }
  }
  
  function process($current, $total) {
    # TODO: fix this hierarchical spaguetti
    
    // All sections will be cached. We need this to set section parents
    // properly in a second step.
    $sections = array();
  
    // Import stuff.
    $vid = variable_get('spip2drupal_vocabulary_sections', NULL);
    $query = "SELECT id_rubrique FROM {rubriques} WHERE statut='publie' LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_rubrique', $row->id_rubrique);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }
      $term = array(
        'vid' => $vid,
        'name' => $data->titre,
        'description' => $data->descriptif
      );
      taxonomy_save_term($term);

      // Store the mapping.
      $record = array('id_rubrique' => $data->id_rubrique, 'tid' => $term['tid']);
      drupal_write_record('spip_sections', $record);
      $sections[$data->id_rubrique] = array('data' => $data, 'term' => $term);

      $this->updateContext();
  }
  
    // Set parents.
    foreach ($sections as $section) {
      $data = $section['data'];
      if ($data->id_parent != '0') {
        $term = $section['term'];
        $parent = $sections[$data->id_parent]['term'];
        $term['parent'] = $parent['tid'];
        taxonomy_save_term($term);
      }
    }
  }
}
