<?php

require_once 'base.batch.inc';

/**
 * Example batch operation.
 */
class spipImportTest extends BaseBatchOperation implements iBatchOperation {
  public $token = 'test';

  function count() {
    $query = 'SELECT COUNT(test) FROM {test}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    $query = "SELECT * FROM {test} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      // do something

      $this->updateContext();
    }
  }
}
