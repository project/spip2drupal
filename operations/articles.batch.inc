<?php

require_once 'base.batch.inc';

/**
 * Batch operation. Import SPIP articles as story nodes.
 * 
 * TODO: "surtitre";"soustitre";"descriptif";"ps";"id_secteur";     "maj";"export";"date_redac";"visites";"referers";"popularite";"accepter_forum";"auteur_modif";"extra";   "lang";"langue_choisie";"id_trad";"nom_site";"url_site";"id_version";"idx";"url_propre"
 *
 */
class spipImportArticles extends BaseBatchOperation implements iBatchOperation {
  public $token = 'articles';

  function __construct(&$context, $step) {
    $this->node_type = variable_get('spip2drupal_node_type_articles', '');
    // For import documents sub operation.
    $this->filesPath = file_directory_path();

    if ((float)variable_get('spip_version', NULL) < 2) {
      $this->documentsQuery = 'SELECT sd.id_document FROM {documents} sd LEFT JOIN {documents_articles} sda ON sd.id_document = sda.id_document WHERE sda.id_article = %d ORDER BY sd.id_document ASC';
    }
    else {
      $this->documentsQuery = 'SELECT id_document FROM {documents_liens} WHERE objet=\'article\' AND id_objet = %d ORDER BY id_document ASC';
    }

    // Daddy does the job.
    parent::__construct($context, $step);
  }

  function count() {
    $query = 'SELECT COUNT(id_article) FROM {articles} ORDER BY id_article ASC';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    // Cache mapping values for current batch iteration.
    $auteurs = array();
    $sections = array();
    $keywords = array();
    // Load all sections now as they are a few.
    $rsc = db_query('SELECT * from {spip_sections}');
    while ($row = db_fetch_object($rsc)) {
      $sections[$row->id_rubrique] = $row->tid;
    }
    // values for below calls to preg_replace().
    $file_pattern = '/IMG(\/\w+)?\/(.*)\//';
    $file_replacement = '/'.file_directory_path().'/$2';

    $query = "SELECT id_article FROM {articles} ORDER BY id_article ASC LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_article', $row->id_article);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }

      // uid.
      if (!isset($auteurs[$data->id_auteur])) {
        $query = 'SELECT uid FROM {spip_users} WHERE id_auteur = %d LIMIT 1';
        $auteurs[$data->id_auteur] = db_result(db_query($query, $data->id_auteur));
      }
      $uid = $auteurs[$data->id_auteur];

      // Taxonomy terms.
      $terms = array();
      // ... for section.
      $terms[] = $sections[$data->id_rubrique];
      // ... for keywords.
      $query = 'SELECT id_mot FROM {mots_articles} WHERE id_article = %d';
      $rs2 = $this->dbQuery($query, $data->id_article) or form_set_error('', "Error in $query.");
      while ($row2 = mysql_fetch_object($rs2)) {
        if (!isset($keywords[$row2->id_mot])) {
          $query = 'SELECT tid FROM {spip_keywords} WHERE id_mot=%d LIMIT 1';
          $keywords[$row2->id_mot] = db_result(db_query($query, $row2->id_mot));
        }
        $terms[] = $keywords[$row2->id_mot];
      }

      // Files.
      $files = $this->importDocuments($data->id_article, $uid);

      $node = (object)array(
        'uid'     => $uid,
        'type'    => $this->node_type,
        'title'   => $data->titre,
        'teaser'  => $data->chapo,
        'body'    => preg_replace($file_pattern, $file_replacement, $data->texte),
        'created' => $data->date,
        'changed' => $data->date_modif,
        'comment' => 1, // (1 = no further comments)
        'format'  => 2, // Full HTML
        'files'   => $files,
        'language' => $data->lang,
        'taxonomy' => $terms,
      );

      $this->setStatus($data->statut, $node);

      node_save($node);
      if ($node->nid) {
        $record = array('id_article' => $data->id_article, 'nid' => $node->nid);
        drupal_write_record('spip_articles', $record);
      }

      $this->updateContext();
    }
  }

  /**
   * Set the node status (published or not) based in spip statut.
   *
   * This is a separate in order to make it clean to override it and implement a
   * more complete moderation mechanism (for example see modr8).
   */
  function setStatus($statut, &$node) {
    // calculate the article status
    switch ($statut) {
       // in the trash
      case 'poubelle':
        $node->status = 0;
        break;
      // published
      case 'publie':
        $node->status = 1;
        break;
      // draft (en cours de redaction)
      case 'prepa':
        $node->status = 0;
        break;
      // proposed to publish
      case 'prop': 
        $node->status = 0;
        break;        
      // refused
      case 'refuse':
        $node->status = 0;
        break;
    }
  }
 
  /**
   * Obtain documents of given article and import them to Drupal.
   */
  function importDocuments($id_article, $uid) {
    $files = array();
    $rsc = $this->dbQuery($this->documentsQuery, $id_article);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_document', $row->id_document);
      if ($data === FALSE) {
        form_set_error('', t('Error importing document %id_document. Related article: %id_article', array('%id_document' => $row->id_document, '%id_article' => $id_article)));
        continue;
      }
      $file = $this->importFile($data->fichier, $data->date, $data->titre, $data->taille);
      if ($file == FALSE) {
        form_set_error('', t('Error importing document %id_document. «%file» does not exist. Related article id: %id_article.', array('%file' => $data->fichier, '%id_article'=> $id_article, '%id_document' => $row->id_document)));
        continue;
      }
      $files[$file['fid']] = $file;
      $record = array('id_document' => $data->id_document, 'fid' => $file['fid']);
      drupal_write_record('spip_documents', $record);
    }

    return $files;
  }

  /**
   * Define a file in Drupal's file table.
   */
  function importFile($filename, $timestamp, $description = '', $filesize = NULL) {
    $src = $this->filesPath.'/'.$filename;
    $filemime = file_get_mimetype($src);
    if (is_null($filesize)) {
      $filesize = filesize($src);
    }
    $file = array(
      'uid'       => $uid,
      'filename'  => $filename,
      'filepath'  => $src,
      'filemime'  => $filemime,
      'filesize'  => $filesize,
      'status'    => FILE_STATUS_TEMPORARY,
      'timestamp' => $timestamp,
    );
    drupal_write_record('files', $file);

    $file['new'] = TRUE;
    $file['list'] = TRUE;
    $file['description'] = $description;

    return $file;
  }
}
