<?php

require_once 'base.batch.inc';

/**
 * Batch operations. Import SPIP keywords as taxonomy terms.
 *
 * TODO: texte, unseul?
 */
class spipImportKeywords extends BaseBatchOperation implements iBatchOperation {
  public $token = 'keywords';

  function count() {
    $query = 'SELECT COUNT(id_mot) FROM {mots}';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    // Cache mapping values for current batch iteration.
    $vocabularies = array();
    // Load all vocabularies now as they are a few.
    $rsc = db_query("SELECT * from {spip_keywords_groups}");
    while ($row = db_fetch_object($rsc)) {
      $vocabularies[$row->id_groupe] = $row->vid;
    }

    $query = "SELECT id_mot FROM {mots} LIMIT $current, ".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_mot', $row->id_mot);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }
      $term = array(
        'vid' => $vocabularies[$data->id_groupe],
        'name' => $data->titre,
        'description' => $data->descriptif
      );
      taxonomy_save_term($term);

      // Store the mapping.
      $record = array('id_mot' => $row->id_mot, 'tid' => $term['tid']);
      drupal_write_record('spip_keywords', $record);

      $this->updateContext();
    }
  }
}
