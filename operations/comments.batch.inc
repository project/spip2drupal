<?php

require_once 'base.batch.inc';

/**
 * Batch operation. Import SPIP comments.
 */
class spipImportComments extends BaseBatchOperation implements iBatchOperation {
  public $token = 'comments';

  function count() {
    $query = 'SELECT COUNT(id_forum) FROM {forum} WHERE id_article != 0';
    $total = mysql_result($this->dbQuery($query), 0);
    return $total;
  }
  
  function process($current, $total) {
    // Cache mapping values for this batch iteration.
    $auteurs = array();
    $articles = array();
    $comments = array();
    $query = "SELECT id_forum FROM {forum} WHERE id_article !=0 LIMIT $current,".$this->step;
    $rsc = $this->dbQuery($query);
    while ($row = mysql_fetch_object($rsc)) {
      $data = $this->fetch('id_forum', $row->id_forum);
      if ($data === FALSE) {
        $this->context['sandbox']['current']++;
        continue;
      }
      // related uid.
      if (!isset($auteurs[$data->id_auteur])) {
        $query = "SELECT uid FROM {spip_users} WHERE id_auteur = %d LIMIT 1";
        $auteurs[$data->id_auteur] = db_result(db_query($query, $data->id_auteur));
      }
      $uid = $auteurs[$data->id_auteur];
      // related name.
      $name = db_result(db_query('SELECT name FROM {users} WHERE uid = %d LIMIT 1', array($uid)));
      // related nid.
      if (!isset($articles[$data->id_article])) {
        $query = "SELECT nid FROM {spip_articles} WHERE id_article = %d LIMIT 1";
        $articles[$data->id_article] = db_result(db_query($query, $data->id_article));
      }
      $nid = $articles[$data->id_article];
      // related parent.
      if (!isset($comments[$data->id_forum])) {
        $query = "SELECT cid FROM {spip_comments} WHERE id_forum = %d LIMIT 1";
        $comments[$data->id_forum] = db_result(db_query($query, $data->id_forum));
      }
      $pcid = $comments[$data->id_forum];

      $comment = array(
        'uid'      => $uid,
        'name'     => $name,
        'hostname' => $data->ip,
        'nid'      => $nid,
        'subject'  => $data->titre,
        'comment'  => $data->texte,
        'format'   => 1, // unfiltered HTML
        'pid'      => $pcid,
        'status'   => ($data->statut=='publie')?1:0,
      );
      // Save the comment.
      $cid = comment_save($comment);
      // sadly the comment_save function sets timestamp to time()...
      db_query("UPDATE {comments} SET timestamp=%d WHERE cid=%d", array($data->date_heure, $cid));
      $record = array('id_forum' => $data->id_forum, 'cid' => $cid);
      drupal_write_record('spip_comments', $record);

      $this->updateContext();
    }
  }
}
