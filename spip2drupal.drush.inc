<?php

/**
 * Return spip's classes of "objects".
 */
function spip2drupal_classes() {
  static $classes = FALSE;
  if (!$classes) {
    $classes = array('articles', 'comments', 'documents', 'keywords', 'keywords_groups', 'sections', 'sites', 'users');
  }
  return $classes;
}

/**
 * Implementation of hook_command().
 */
function spip2drupal_drush_command() {
  $items = array();

  $items['spip2drupal-id'] = array(
    'description' => 'Get the drupal id for a given spip object id.',
    'arguments' => array(
      'id' => 'The object id.',
    ),
    'options' => array(
      'class' => 'The class of the object to get the id. Valid classes are: '.implode(',', spip2drupal_classes())
    ),
    'aliases' => array('s2d-id'),
  );

  return $items;
}

/**
 * Command callback.
 */
function drush_spip2drupal_id($id) {
  $class = drush_get_option('class', 'articles');

  $table = 'spip_'.$class;
  $schema = drupal_get_schema($table, TRUE);
  $fields = array_keys($schema['fields']);
  $query = 'SELECT '.$fields[1].' FROM {'.$table.'} WHERE '.$fields[0].' = %d';
  $row = db_result(db_query($query, $id));
  if ($row === FALSE) {
    drush_log(dt('Object !c !o does not exist.', array('!o' => $id, '!c' => $class)), 'error');
  }
  else {
    drush_log(dt('Drupal !f: !r.', array('!f' => $fields[1], '!r' => $row)), 'ok');
  }
}

function drush_spip2drupal_id_validate() {
  $args = func_get_args();
  if (count($args) == 0) {
    return drush_set_error('SPIP', dt('Missing argument.'));
  }
  $class = drush_get_option('class', 'articles');
  $classes = spip2drupal_classes();
  if (!in_array($class, $classes)) {
    return drush_set_error('SPIP', dt('Valid classes are !c.', array('!c' => $classes)));
  }
}
